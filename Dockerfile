FROM registry.gitlab.com/adrift/games/docker-steamcmd

USER root

COPY files/start.sh .
RUN apt update -y \
 && apt install unzip -y \
 && chown -R steam:steam /home/steam \
 && chmod +x start.sh

USER steam

ENTRYPOINT [ "./start.sh" ]
