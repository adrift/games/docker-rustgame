# docker-rustgame

A Rust (https://rust.facepunch.com/) dedicated server, containerized

Powered by **https://gitlab.com/adrift/games/docker-steamcmd**

## quick start

1. Quickly test image operation
```
docker run --rm registry.gitlab.com/adrift/games/docker-rustgame
```

2. Run a Rust Server, custom name as `rust1`, and volume out your server data for persistence
```
mkdir ~/rust1
chown -R 5714:5714 ~/rust1
docker run --rm -v ~/rust1:/home/steam/rust registry.gitlab.com/adrift/games/docker-rustgame
```

3. Manually shell into your existing server container for fun and profit
```
#optionally stop, most likely necessary for most things you'll want to do
docker stop yourcontainername
docker run --entrypoint "/bin/bash" -v ~/rust1:/home/steam/rust registry.gitlab.com/adrift/games/docker-rustgame
```

## considerations

1. Upgrades aren't supported yet. Although, upon runtime steamcmd should be invoked with `+validate` which we expect to run the update check. Still, be aware that in some cases you'll need to shell into your container to make it do what you need.

2. The readme always recommends running with `--rm`. This is because the only thing that is relevant keep around are already persisted through the volume. Although there is minimal benefit to keeping the actual container around, you may choose to do so. It's the internet: you do you, friend.
