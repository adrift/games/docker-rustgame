#!/bin/bash

steamcmd +login anonymous +force_install_dir /home/steam/rust +app_update 258550 validate +quit

[[ -z $SERVER_IP ]] && SERVER_IP="0.0.0.0"
[[ -z $SERVER_PORT ]] && SERVER_PORT="28015"
[[ -z $RCON_IP ]] && RCON_IP="0.0.0.0"
[[ -z $RCON_PORT ]] && RCON_PORT="28016"
[[ -z $RCON_PASSWORD ]] && RCON_PASSWORD=""
[[ -z $SERVER_MAXPLAYERS ]] && SERVER_MAXPLAYERS="50"
[[ -z $SERVER_HOSTNAME ]] && SERVER_MAXPLAYERS="rust | gitlab.com/adrift/games/docker-rustgame"
[[ -z $SERVER_LEVEL ]] && SERVER_LEVEL="Procedural Map"
[[ -z $SERVER_SEED ]] && SERVER_SEED=""
[[ -z $SERVER_WORLDSIZE ]] && SERVER_WORLDSIZE="4800"
[[ -z $SERVER_DESCRIPTION ]] && SERVER_DESCRIPTION="A Rust server powered by gitlab.com/adrift/games/docker-rustgame"
[[ -z $SERVER_GLOBALCHAT ]] && SERVER_GLOBALCHAT="true"
[[ -z $SERVER_HEADERIMAGE ]] && SERVER_HEADERIMAGE="https://assets.gitlab-static.net/uploads/-/system/group/avatar/3270825/a4c1597e007d22f17599761d3f4b382d.png?width=64"
[[ -z $SERVER_URL ]] && SERVER_URL="gitlab.com/adrift/games/docker-rustgame"

exec ./rust/RustDedicated -batchmode -nographics \
  -server.ip $SERVER_IP \
  -server.port $SERVER_PORT \
  -rcon.ip $RCON_IP \
  -rcon.port $RCON_PORT \
  -rcon.password $RCON_PASSWORD \
  -server.maxplayers $SERVER_MAXPLAYERS \
  -server.hostname $SERVER_MAXPLAYERS \
  -server.level $SERVER_LEVEL \
  -server.seed $SERVER_SEED \
  -server.worldsize $SERVER_WORLDSIZE \
  -server.saveinterval 300 \
  -server.globalchat $SERVER_GLOBALCHAT \
  -server.description $SERVER_DESCRIPTION \
  -server.headerimage $SERVER_HEADERIMAGE \
  -server.url $SERVER_URL
